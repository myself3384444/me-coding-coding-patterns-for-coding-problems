title: Coding Patterns

## Intro

This course categorizes coding interview problems into a **set of 27 patterns**. Each pattern will be a complete tool - consisting of data structures, algorithms, and analysis techniques - to solve a specific category of problems.

- We have chosen each problem carefully such that it not only maps to the same pattern but also presents different constraints. Overall, the course has around 200 problems mapped to 22 patterns.
- We will start with a brief introduction of each pattern before jumping onto the problems. Under each pattern, the first problem will explain the underlying pattern in detail to build the concepts that can be applied to later problems. The later problems will focus on the different constraints each problem presents and how our algorithm needs to change to handle them.